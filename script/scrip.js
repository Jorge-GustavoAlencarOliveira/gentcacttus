function hiddenscrollActivemenu(){
  const allbody = document.querySelector("body");
  const menusanduiche = document.querySelector('.navbar-toggler');
  
  function esconderScroll(){
    allbody.classList.add('scrollhidden');
  }
  menusanduiche.addEventListener('click', esconderScroll);

}
hiddenscrollActivemenu();


function outsideclickMenu() {  
  const html = document.documentElement;
  const menu = document.querySelector(".navbar-collapse");
  const allbody = document.querySelector("body");
  const menusanduiche = document.querySelector('.navbar-toggler-icon');  
  
  function outsideclick (event){
    if(event.target != menu && event.target.parentNode != menu && event.target != menusanduiche){     
      menu.classList.remove('show');       
      allbody.classList.remove('scrollhidden')
    }
  }
  html.addEventListener('click', outsideclick)
}
outsideclickMenu();


function activeCarousel (){
  const buttoncarosel = document.querySelectorAll("[data-bs-target='#carouselExampleIndicators']");
  
  function removeActive () {
    buttoncarosel.forEach(e=>{
      e.classList.remove("active")
    })

  }
  function active (){
    removeActive();   
    this.classList.add("active");
  }
  
  buttoncarosel.forEach(e =>{
    e.addEventListener('click', active)
  })
}
activeCarousel();

function dropdownMenu (){
  const buttondropdown = document.querySelector(".dropdown1");
  const dropdownmenu = document.querySelector('.dropdownmenu');
  const btnfechar = document.querySelector('.btnfechar');  
  
  function showDropdownmenu(){
    dropdownmenu.classList.add('show')
  }
  function hidebtn () {
    this.classList.add("sumirbtn");
    showDropdownmenu();
  }
   function hideDropdownmenu (){
    dropdownmenu.classList.remove('show');
    buttondropdown.classList.remove('sumirbtn')
   }
  if (buttondropdown != null, btnfechar != null ){
    buttondropdown.addEventListener('click', hidebtn);
    btnfechar.addEventListener('click', hideDropdownmenu);
  }
}
dropdownMenu();

